package com.RIcommons.entity.pk;

import jakarta.persistence.Embeddable;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Embeddable
@Getter
@Setter
public class TecnicoEspecialidadPK implements Serializable
{
    private Integer id_tecnico;
    private Integer id_especialidad;

}
