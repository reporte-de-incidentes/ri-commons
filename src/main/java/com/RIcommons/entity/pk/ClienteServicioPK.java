package com.RIcommons.entity.pk;

import jakarta.persistence.Embeddable;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Embeddable
@Setter
@Getter
public class ClienteServicioPK implements Serializable
{
    private Integer id_cliente;

    private Integer id_servicio;

}
