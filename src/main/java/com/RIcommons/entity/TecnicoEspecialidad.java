package com.RIcommons.entity;

import com.RIcommons.entity.pk.TecnicoEspecialidadPK;
import jakarta.persistence.*;
import lombok.Data;

@Entity
@Table(name = "TECNICO_ESPE")
@Data
public class TecnicoEspecialidad
{
    @EmbeddedId
    private TecnicoEspecialidadPK id_tecnicoEspecialidad;

    @ManyToOne
    @JoinColumn(name = "id_tecnico", insertable = false, updatable = false)
    private Tecnico tecnico;

    @ManyToOne
    @JoinColumn(name = "id_especialidad", insertable = false, updatable = false)
    private Especialidad especialidad;


}
