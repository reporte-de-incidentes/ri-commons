package com.RIcommons.entity;

import jakarta.persistence.*;
import lombok.Data;

import java.util.List;

@Entity
@Table(name = "CLIENTE")
@Data
public class Cliente
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id_cliente;

    @Column(name = "nombre", length = 255)
    private String nombre;

    @Column(name = "dni")
    private int dni;

    @Column(name = "email", length = 75)
    private String email;

    @Column(name = "nro_contacto")
    private int nro_contacto;

    @OneToMany(mappedBy = "cliente")
    private List<Incidente> listaIncidentes;

    @OneToMany(mappedBy = "cliente")
    private List<ClienteServicio> listaServicios;


}
