package com.RIcommons.entity;

import jakarta.persistence.*;
import lombok.Data;

import java.util.List;

@Entity
@Table(name = "ESPECIALIDAD")
@Data
public class Especialidad
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id_especialidad;

    @Column(name = "tipo", length = 255)
    private String tipo;

    @ManyToOne
    @JoinColumn(name = "id_servicio", referencedColumnName = "id_servicio")
    private Servicio servicio;

    @OneToMany(mappedBy = "especialidad")
    private List<Incidente> listaIncidentes;

}
