package com.RIcommons.entity;

import jakarta.persistence.*;
import lombok.Data;

import java.util.List;

@Entity
@Table(name = "TECNICO")
@Data
public class Tecnico
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id_tecnico;

    @Column(name = "nombre", length = 50)
    private String nombre;

    @Column(name = "email", length = 75)
    private String email;

    @Column(name = "nro_contacto")
    private int nro_contacto;

    @OneToMany(mappedBy = "tecnico")
    private List<Incidente> listaIncidentes;

}
