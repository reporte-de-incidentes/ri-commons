package com.RIcommons.entity;

import jakarta.persistence.*;
import lombok.Data;

import java.time.LocalDateTime;

@Entity
@Table(name = "INCIDENTE")
@Data
public class Incidente
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id_incidente;

    @Column(name = "descripcion", length = 500)
    private String descripcion;

    @Column(name = "estado", length = 50)
    private String estado;

    @Column(name = "dia_ingresado")
    private LocalDateTime dia_ingresado;

    @Column(name = "dia_finalizado")
    private LocalDateTime dia_finalizado;

    @Column(name = "tiempo_estimado")
    private int tiempo_estimado;

    @Column(name = "tiempo_colchon")
    private int tiempo_colchon;

    @Column(name = "tiempo_finalizacion")
    private int tiempo;

    @ManyToOne
    @JoinColumn(name = "id_servicio", referencedColumnName = "id_servicio")
    private Servicio servicio;

    @ManyToOne
    @JoinColumn(name = "id_especialidad", referencedColumnName = "id_especialidad")
    private Especialidad especialidad;

    @ManyToOne
    @JoinColumn(name = "id_tecnico", referencedColumnName = "id_tecnico")
    private Tecnico tecnico;

    @ManyToOne
    @JoinColumn(name = "id_cliente", referencedColumnName = "id_cliente")
    private Cliente cliente;

}
