package com.RIcommons.entity;

import jakarta.persistence.*;
import lombok.Data;

import java.util.List;

@Entity
@Table(name = "SERVICIO")
@Data
public class Servicio
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id_servicio;

    @Column(name = "tipo", length = 255)
    private String tipo;

    @OneToMany(mappedBy = "servicio")
    private List<Especialidad> listaEspecialidades;

    @OneToMany(mappedBy = "servicio")
    private List<Incidente> listaIncidentes;

}
