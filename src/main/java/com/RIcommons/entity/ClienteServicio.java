package com.RIcommons.entity;

import com.RIcommons.entity.pk.ClienteServicioPK;
import jakarta.persistence.*;
import lombok.Data;

@Entity
@Table(name = "CLIENTE_SERVICIO")
@Data
public class ClienteServicio
{
    @EmbeddedId
    private ClienteServicioPK id_clienteServicio;

    @ManyToOne
    @JoinColumn(name = "id_cliente", insertable = false, updatable = false)
    private Cliente cliente;

    @ManyToOne
    @JoinColumn(name = "id_servicio", insertable = false, updatable = false)
    private Servicio servicio;

}
