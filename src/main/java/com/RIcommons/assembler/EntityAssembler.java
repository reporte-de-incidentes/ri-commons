package com.RIcommons.assembler;

import com.RIcommons.dto.ClienteDTO;
import com.RIcommons.dto.IncidenteDTO;
import com.RIcommons.dto.TecnicoDTO;
import com.RIcommons.entity.Cliente;
import com.RIcommons.entity.Incidente;
import com.RIcommons.entity.Tecnico;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

@Component
public class EntityAssembler
{
    public Incidente toIncidente(IncidenteDTO incidenteDTO){
        Incidente incidente = new Incidente();
        if (!ObjectUtils.isEmpty(incidenteDTO)){
            incidente.setDescripcion(incidenteDTO.getDescripcion());
            incidente.setTiempo_estimado(incidenteDTO.getTiempo_estimado());
            incidente.setTiempo_colchon(incidenteDTO.getTiempo_colchon());
        }
        return incidente;
    }

    //TENGO QUE HACER LAS DEMAS ENTITY, PARA CLIENTE Y TECNICO

    public Cliente toCliente(ClienteDTO clienteDTO){
        Cliente cliente = new Cliente();
        if (!ObjectUtils.isEmpty(clienteDTO)){
            cliente.setNombre(clienteDTO.getNombre());
            cliente.setDni(clienteDTO.getDni());
            cliente.setEmail(clienteDTO.getEmail());
        }
        return cliente;
    }

    public Tecnico toTecnico(TecnicoDTO tecnicoDTO){
        Tecnico tecnico = new Tecnico();
        if (!ObjectUtils.isEmpty(tecnicoDTO)){
            tecnico.setNombre(tecnicoDTO.getNombre());
            tecnico.setEmail(tecnicoDTO.getEmail());
            tecnico.setNro_contacto(tecnicoDTO.getNro_contacto());
        }
        return tecnico;
    }

}
