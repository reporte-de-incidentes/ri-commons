package com.RIcommons.assembler;

import com.RIcommons.dto.*;
import com.RIcommons.entity.*;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

@Component
public class DTOsAssembler
{
    public ClienteDTO toClienteDTO(Cliente cliente){
        ClienteDTO clienteDTO = new ClienteDTO();
        if (!ObjectUtils.isEmpty(cliente)){
            clienteDTO.setNro_cliente(cliente.getId_cliente());
            clienteDTO.setNombre(cliente.getNombre());
            clienteDTO.setDni(cliente.getDni());
            clienteDTO.setEmail(cliente.getEmail());
            clienteDTO.setNro_contacto(cliente.getNro_contacto());
        }
        return clienteDTO;
    }


    public EspecialidadDTO toEspecialidadDTO(Especialidad especialidad){
        EspecialidadDTO especialidadDTO = new EspecialidadDTO();
        if (!ObjectUtils.isEmpty(especialidad)){
            especialidadDTO.setNro_especialidad(especialidad.getId_especialidad());
            especialidadDTO.setTipo(especialidad.getTipo());
        }
        return especialidadDTO;
    }


    public ServicioDTO toServicioDTO(Servicio servicio){
        ServicioDTO servicioDTO = new ServicioDTO();
        if (!ObjectUtils.isEmpty(servicio)){
            servicioDTO.setNro_servicio(servicio.getId_servicio());
            servicioDTO.setTipo(servicio.getTipo());
        }
        return servicioDTO;
    }

    public TecnicoDTO toTecnicoDTO(Tecnico tecnico){
        TecnicoDTO tecnicoDTO = new TecnicoDTO();
        if (!ObjectUtils.isEmpty(tecnico)){
            tecnicoDTO.setNro_tecnico(tecnico.getId_tecnico());
            tecnicoDTO.setNombre(tecnico.getNombre());
            tecnicoDTO.setEmail(tecnico.getEmail());
            tecnicoDTO.setNro_contacto(tecnico.getNro_contacto());
        }
        return tecnicoDTO;
    }


    public IncidenteDTO incidenteDTO(Incidente incidente){
        IncidenteDTO incidenteDTO = new IncidenteDTO();
        if (!ObjectUtils.isEmpty(incidente)){
            incidenteDTO.setNro_incidente(incidente.getId_incidente());
            incidenteDTO.setDescripcion(incidente.getDescripcion());
            incidenteDTO.setTiempo_estimado(incidente.getTiempo_estimado());
            incidenteDTO.setTiempo_colchon(incidente.getTiempo_colchon());
            incidenteDTO.setServicio(incidente.getServicio().getId_servicio());
            incidenteDTO.setEspecialidad(incidente.getEspecialidad().getId_especialidad());
            incidenteDTO.setTecnico(incidente.getTecnico().getId_tecnico());
            incidenteDTO.setCliente(incidente.getCliente().getId_cliente());
        }
        return incidenteDTO;
    }

}
