package com.RIcommons.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IncidenteDTO
{
    private int nro_incidente;
    private String descripcion;
    private String estado;
    private int tiempo_estimado;
    private int tiempo_colchon;
    private int tiempo;
    private Integer servicio;
    private Integer tecnico;
    private Integer especialidad;
    private Integer cliente;

    public IncidenteDTO(){}

}
