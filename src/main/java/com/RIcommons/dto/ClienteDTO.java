package com.RIcommons.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ClienteDTO
{
    private int nro_cliente;
    private String nombre;
    private int dni;
    private String email;
    private int nro_contacto;

    public ClienteDTO(){}

}
