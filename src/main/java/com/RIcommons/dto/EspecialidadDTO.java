package com.RIcommons.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EspecialidadDTO
{
    private int nro_especialidad;
    private String tipo;

    public EspecialidadDTO(){}

}
