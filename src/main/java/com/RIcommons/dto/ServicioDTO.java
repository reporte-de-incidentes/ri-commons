package com.RIcommons.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ServicioDTO
{
    private int nro_servicio;
    private String tipo;

    public ServicioDTO(){}

}
