package com.RIcommons.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TecnicoDTO
{
    private int nro_tecnico;
    private String nombre;
    private String email;
    private int nro_contacto;

    public TecnicoDTO(){}

}
